import {Entity, Column, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class PersonProfile {
    @PrimaryGeneratedColumn()
    person_profile_id: number;
    @Column()
    name: string;
    @Column()
    url: string;
    @Column
    work_experience: string;
    @Column
    education: string;
    @Column
    skill_set_id: number;
    @Column
    connections: string;

}