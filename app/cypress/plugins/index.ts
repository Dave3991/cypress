const mysql = require('mysql')

function queryTestDb(query, config) {
    console.log(config);
    const connection = mysql.createConnection(config.env.db);
    connection.connect();
    return new Promise((resolve, reject) => {
        connection.query(query, (error, results) => {
            if (error) reject(error)
            else {
                connection.end()
                return resolve(results)
            }
        })
    })
}

module.exports = (on, config) => {
    // Usage: cy.task('queryDb', query)
    on('task', {
        queryDb: query => {
            return queryTestDb(query, config)
        },
    })
    console.log(config);
}