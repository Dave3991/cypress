MIN_MAKE_VERSION := 3.82

ifneq ($(MIN_MAKE_VERSION),$(firstword $(sort $(MAKE_VERSION) $(MIN_MAKE_VERSION))))
	$(error GNU Make $(MIN_MAKE_VERSION) or higher required)
endif

.DEFAULT_GOAL:=help

##@ Development
.PHONY: up down start-watch info cypress-run
up: ## Start the application containers (use "args=" to supply custom arguments)
	xhost + && docker-compose -f "./docker/docker-compose.yml" up $(args) && xhost -
down: ## Stop and remove the application containers
	docker-compose -f "./docker/docker-compose.yml" down --volumes
info: ## show cypress info
	docker-compose -f "./docker/docker-compose.yml" exec cypress_app cypress info
cli: ## goes to running container cli
	docker-compose -f "./docker/docker-compose.yml" exec cypress_app bash

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
